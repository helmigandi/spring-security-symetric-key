package io.github.helmigandi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringSecuritySymetricKeyApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringSecuritySymetricKeyApplication.class, args);
	}

}
