package io.github.helmigandi.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/home")
public class HomeController {

    @GetMapping
    @PreAuthorize("hasAuthority('SCOPE_READ')")
    public String home(Authentication auth) {
        return "Hello, " + auth.getName() + ". Authorities: " + auth.getAuthorities();
    }

    @GetMapping("/update")
    public String updateOnly(Authentication auth) {
        return "Hello, " + auth.getName() + ". Authorities: " + auth.getAuthorities();
    }
}
